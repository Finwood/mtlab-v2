FROM fedora:latest

RUN dnf -y upgrade && dnf clean all

RUN dnf -y install texlive-scheme-full biber make && \
    dnf -y install python3-{pygments,numpy,scipy,matplotlib,pandas,sympy} && \
    dnf clean all

ADD https://raw.githubusercontent.com/aclements/latexrun/master/latexrun /usr/bin/
RUN chmod +x /usr/bin/latexrun

RUN dnf -y install inkscape
