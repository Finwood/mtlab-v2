TARGET         ?= tex/main

LATEXRUN       ?= latexrun
RM             ?= /usr/bin/rm
BASH           ?= /usr/bin/bash
PYTHON         ?= /usr/bin/python3

# Inkscape svg handling
INKSCAPE       ?= /usr/bin/inkscape
INKSCAPE_ARGS  ?= --export-latex --export-area-page
SVGDIR         ?= img
SVGFILES       ?= $(basename $(notdir $(wildcard $(SVGDIR)/*.svg)))
SVGDEPS         = $(addsuffix .pdf,$(addprefix ,$(SVGFILES))) \
                  $(addsuffix .pdf_tex,$(addprefix ,$(SVGFILES)))

# tex file templates
SH_TEX_TEMPLATES       ?= $(shell find -type f -name '*.tex.sh')
RENDERED_SH_TEMPLATES   = $(SH_TEX_TEMPLATES:%.tex.sh=%.tex)
PY_TEX_TEMPLATES       ?= $(shell find -type f -name '*.tex.py')
RENDERED_PY_TEMPLATES   = $(PY_TEX_TEMPLATES:%.tex.py=%.tex)

TEMPLATES               = $(SH_TEX_TEMPLATES) $(PY_TEX_TEMPLATES)
RENDERED_TEMPLATES      = $(RENDERED_SH_TEMPLATES) $(RENDERED_PY_TEMPLATES)

.PHONY: pdf svgs templates run-anyway
pdf: $(TARGET).pdf
svgs: $(SVGDEPS)
templates: $(RENDERED_TEMPLATES)

%.pdf: %.tex run-anyway templates svgs
	TEXINPUTS="tex//:lib//:$(TEXINPUTS):" $(LATEXRUN) --bibtex-cmd biber $<

.PHONY: clean
clean:
	$(LATEXRUN) --clean-all
	$(RM) -rf $(SVGDEPS) $(RENDERED_TEMPLATES)

%.pdf %.pdf_tex: $(SVGDIR)/%.svg
	$(INKSCAPE) $(INKSCAPE_ARGS) --file=$< --export-pdf=$@

tex/build.tex: run-anyway
%.tex: %.tex.sh
	$(BASH) $< > $@

%.tex: %.tex.py
	$(PYTHON) $< > $@
