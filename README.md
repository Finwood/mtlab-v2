# Messtechnisches Labor • Versuch 2: Lock-In-Verstärker

Bla bla bla, Messtechnik etc.

## GitLab CI
[![pipeline status][]][pipelines]

Dieses Dokument wird nach jedem Update von [GitLab CI][] neu kompiliert, sodass
[hier][pdf] jeweils die aktuelle Version verfügbar ist.
Wen es interessiert: die zugehörige Konfiguration ist in der
[`.gitlab-ci.yml`](.gitlab-ci.yml) zu finden.

Sobald es erste _Releases_ gibt, werden diese auf der [_Tags_](/../tags)-Seite
den _Release Notes_ angehängt sein.


  [pipelines]:        /../pipelines
  [pipeline status]:  https://gitlab.com/Finwood/mtlab-v2/badges/master/pipeline.svg
  [pdf]:              https://gitlab.com/Finwood/mtlab-v2/-/jobs/artifacts/master/file/latex.out/main.pdf?job=compile
  [artifacts]:        https://gitlab.com/Finwood/mtlab-v2/-/jobs/artifacts/master/browse?job=compile
  [GitLab CI]:        https://docs.gitlab.com/ce/ci/
