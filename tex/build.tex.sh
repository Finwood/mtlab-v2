#!/usr/bin/bash

# DRAFT: 'true' or 'false'
# VERSION: string, arbitrary
# DATEFORMAT, TIMEFORMAT: string, see date(1)

if [ "$DRAFT" != "true" ]
then
  DRAFT=false
fi
cat << EOF
\\newif\\ifdraft
\\draft$DRAFT
EOF

if ! (which git > /dev/null 2>&1)
then
  # create git dummy function
  function git {
    return 0;
  }
fi

COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-parse HEAD 2>/dev/null)}"
COMMIT_REF_NAME="${CI_COMMIT_REF_NAME:-$(git rev-parse --abbrev-ref HEAD 2>/dev/null)}"
COMMIT_TAG="${CI_COMMIT_TAG:-$(git tag --points-at 2>/dev/null)}"
DIRTY=$([[ $(git diff --shortstat 2>/dev/null | tail -n1) != "" ]] && echo "*")
COMMIT_SHA_SHORT="${COMMIT_SHA:0:7}$DIRTY"

if [ -n $CI_PROJECT_URL ]
then
  COMMIT_URL="$CI_PROJECT_URL/tree/${COMMIT_TAG:-$COMMIT_SHA}"
fi

cat << EOF
\\newcommand{\\commitSha}{$COMMIT_SHA}
\\newcommand{\\commitShaShort}{$COMMIT_SHA_SHORT}
\\newcommand{\\commitRefName}{$COMMIT_REF_NAME}
\\newcommand{\\commitTag}{$COMMIT_TAG}
\\newcommand{\\commitUrl}{$COMMIT_URL}
EOF

DATE=$(date "+${DATEFORMAT:-%Y-%m-%d}")
TIME=$(date "+${TIMEFORMAT:-%H:%M}")
COMMIT_DATE=$(date --date="@$(git show -s --format=%ct 2>/dev/null)" "+${DATEFORMAT:-%Y-%m-%d}" 2>/dev/null)
COMMIT_TIME=$(date --date="@$(git show -s --format=%ct 2>/dev/null)" "+${TIMEFORMAT:-%H:%M}" 2>/dev/null)
cat << EOF
\\newcommand{\\builddate}{$DATE}
\\newcommand{\\buildtime}{$TIME}
\\newcommand{\\commitDate}{$COMMIT_DATE}
\\newcommand{\\commitTime}{$COMMIT_TIME}
EOF

VERSION=${VERSION:-${COMMIT_TAG:-"$COMMIT_SHA_SHORT ($COMMIT_REF_NAME)"}}
echo "\\newcommand{\\version}{$VERSION}"
