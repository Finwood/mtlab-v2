\section{Messung der optischen Dichte von Graufiltern}
Ziel dieses Versuches ist die Bestimmung der Dämpfung verschiedenen optischer
Filter. Dabei sollen zwei verschiedene Messverfahren realisiert und miteinander
verglichen werden.

\subsection{Aufbau}
Ein Fotodetektor ist derartig vor einer Laserdiode positioniert, dass sich
dazwischen eine Messstrecke ergibt in welche sich ein Graufilter bringen lässt.
Der Aufbau ist in \fig{1-aufbau} skizziert. Im Fotodetektor befindet sich
ein Transimpedanzverstärker (\fig{TIA}), welcher den Fotostrom in eine einfacher
messbare Spannung $U_{TIA}$ umwandelt. Diese Spannung wird von dem an einen PC
angeschlossenen Lock-In-Verstärker gemessen und aufgezeichnet.
Der Grad der Dämpfung des Graufilters, genannt \textit{optische Dichte}, soll
in diesem Versuch ermittelt werden.

\begin{figure}
	\centering
    \def\svgwidth{50mm}
	\input{TIA.pdf_tex}
    \caption[Transimpedanzverstärker]%
            {Im Fotodetektor verwendeter Transimpedanzverstärker.
             Der durch den Fotoeffekt hervorgerufene Fotostrom der Fotodiode
             wird in eine Spannung $U_{TIA}$ umgewandelt, welche einfacher
             gemessen werden kann.}
    \label{fig:TIA}
\end{figure}
\begin{figure}
	\centering
	\input{1-aufbau.pdf_tex}
    \caption[Messaufbau Erfassung optische Dichte]%
            {Messaufbau zur Erfassung der optischen Dichte von Graufiltern,
             wobei die Laserdiode wahlweise mit dem Lock-In-Verstärker oder einer
             Gleichstromquelle verbunden werden kann \cite{skript}}
    \label{fig:1-aufbau}
\end{figure}

\subsection{Messung mit Gleichlicht}

\subsubsection{Durchführung}
Zunächst sollen die Messungen mit Gleichlicht realisiert werden. Dazu wird die
Laserdiode direkt mit der Gleichspannungsquelle verbunden.
Nachdem eine Referenzmessung des ungedämpften Lasers durchgeführt wurde, werden
nacheinander die verschiedenen zu vermessenden Graufilter montiert und die
Ausgangsspannung des Fotodetektors notiert.
Zu jeder Messung%
\footnote{\SI{16.3}{\kilo\sample} bei $f_S = \SI{29.3}{\kilo\hertz}$}
sind Mittelwert und Standardabweichung zu notieren, darüber
hinaus wird sukzessive über eine, zehn und 100 Messungen gemittelt.
Die Ergebnisse der Messungen sind in \tab{messungen-gleichlicht} dargestellt.

\begin{table}
	\centering
	\caption{Messungen mit Gleichlicht}
    \label{tab:messungen-gleichlicht}
    \small
    \begin{subtable}{.3\linewidth}
        \caption{keine Mittelung}
        \begin{tabu}{ c c c }
            \toprule
                & $ \mu $ & $ \sigma $ \\
            \midrule
                \textbf{OD0} & 2.495  \si{\volt}       & 1   \si{\milli\volt} \\
                \textbf{OD2} & 28.8   \si{\milli\volt} & 200 \si{\micro\volt} \\
                \textbf{OD3} \\
                \textbf{OD4} \\
            \bottomrule
        \end{tabu}
    \end{subtable}
    \begin{subtable}{.3\linewidth}
        \caption{Mittelung über 10 Werte}
        \begin{tabu}{ c c c }
            \toprule
                & $ \mu $ & $ \sigma $ \\
            \midrule
                \textbf{OD0} & 2.502  \si{\volt}       & 300 \si{\micro\volt} \\
                \textbf{OD2} & 28.15  \si{\milli\volt} & 25  \si{\micro\volt} \\
                \textbf{OD3} & 6.35   \si{\milli\volt} & 27  \si{\micro\volt} \\
                \textbf{OD4} & 4.25   \si{\milli\volt} & 25  \si{\micro\volt} \\
            \bottomrule
        \end{tabu}
    \end{subtable}
    \begin{subtable}{.3\linewidth}
        \caption{Mittelung über 10 Werte}
        \begin{tabu}{ c c c }
            \toprule
                & $ \mu $ & $ \sigma $ \\
            \midrule
                \textbf{OD0} & 2.5153 \si{\volt}       & 80  \si{\micro\volt} \\
                \textbf{OD2} & 28.15  \si{\milli\volt} & 5   \si{\micro\volt} \\
                \textbf{OD3} \\
                \textbf{OD4} \\
            \bottomrule
        \end{tabu}
    \end{subtable}
\end{table}

Zur Abschätzung des Einflusses des Umgebungslichts wird anschließend noch eine
Messung bei abgeschalteter Signalquelle durchgeführt.
Über zehn Einzelmessungen gemittelt ergibt sich für den Umgebungslichteinfluss
$\mu = \SI{4.04}{\milli\volt}$ bei einer Standardabweichung von $\sigma =
\SI{25}{\micro\volt}$.

\subsubsection{Auswertung}

\begin{figure}
    \centering
    \begin{subfigure}{2.5in}
        \centering
        \includegraphics[scale=.5]{1a-gleichlicht-OD}
        \subcaption{Messwerte und berechnete Spannung
                    für verschiedene Graufilter}
        \label{fig:1a-OD}
    \end{subfigure}
    \begin{subfigure}{2in}
        \centering
        \includegraphics[scale=.5]{1a-gleichlicht-mu-sigma}
        \subcaption{Mittelwert und $2\sigma$-Umgebung
                    bei Variation der Mittelungsanzahl}
        \label{fig:1a-mu-sigma}
    \end{subfigure}
    \caption[Messung der optischen Dichte mit Gleichlicht]%
            {Messung der optischen Dichte mit Gleichlicht.
             Gerade bei hoher optischer Dichte und somit geringer Signalspannung
             herrscht eine große Diskrepanz zwischen Messwert und erwartetem
             Signal. Es liegt die Vermutung nahe, dass es sich hier um eine
             systematische Messabweichung handelt.}
    \label{fig:1a}
\end{figure}

Die in \tab{messungen-gleichlicht} aufgelisteten Spannungsmesserte sind in
\fig{1a} grafisch dargestellt. Dabei zeigt \fig{1a-OD} die über zehn Messungen
gemittelten Spannungen für die verschiedenen Filter, \fig{1a-mu-sigma} den
Einfluss der Mittelungsanzahl auf die Standardabweichung bzw. die
$2\sigma$-Umgebung.

Aus \fig{1a-OD} wird deutlich, dass offensichtlich eine starke Messabweichung
vorliegt: gerade bei den Messungen der Filter OD3 und OD4 weicht die gemessene
Spannung des Fotodetektors stark vom prognostizierten Ergebnis ab. Da dies
insbesondere bei hoher Dämpfung und somit kleinem Messsignal auftritt liegt die
Vermutung nahe, dass entweder das untere Ende der Messauflösung erreicht ist
oder eine bei kleinen Signalstärken auftretende systematische Messabweichung
vorliegt, wie beispielsweise der Einfluss des Umgebungslichts.

In \fig{1a-mu-sigma} ist erkennbar, dass die Standardabweichung der Messungen
erwartungsgemäß bei Erhöhen der Mittelungsanzahl kleiner wird.
Das obere Diagramm lässt zudem darauf schließen, dass bei hoher Signalintensität
eine zusätzliche Messabweichung auftritt. Der Messwert scheint deutlich von der
Anzahl der Mittelungen abzuhängen, der jeweilige Mittelwert liegt weit außerhalb
der $2\sigma$-Intervalle (\SI{95}{\percent}-Umgebung) der anderen Messungen.
Ein Grund hierfür könnte ein Erwärmen der Messschaltung (vgl. \fig{TIA}) sein,
wodurch bei einer länger andauernden Messung beispielsweise der Wert des
Rückkopplungswiderstands ansteigen könnte.
Die genaue Ursache hierfür ist jedoch nicht Bestandteil dieser Arbeit und soll
daher an dieser Stelle nicht weiter verfolgt werden.

\begin{figure}
    \centering
    \begin{subfigure}{2.5in}
        \centering
        \includegraphics[scale=.5]{1b-gleichlicht-kompensiert-OD}
        \subcaption{Messwerte und berechnete Spannung
                    für verschiedene Graufilter}
        \label{fig:1b-OD}
    \end{subfigure}
    \begin{subfigure}{2in}
        \centering
        \includegraphics[scale=.5]{1b-gleichlicht-kompensiert-mu-sigma}
        \subcaption{Mittelwert und $2\sigma$-Umgebung
                    bei Variation der Mittelungsanzahl}
        \label{fig:2a-mu-sigma}
    \end{subfigure}
    \caption[Messung optische Dichte mit Gleichlicht,
             Kompensation Umgebungslicht]%
            {Messung der optischen Dichte mit Gleichlicht bei Kompensation des
             systematischen Einflusses des Umgebungslichts.
             Durch statische Kompensation des Einflusses des Umgebungslichts
             (dargestellt durch die horizontale gestrichelte Linie) lassen sich
             die Messergebnisse deutlich verbessern, das erwartete Ergebnis
             liegt nun vollständig in der \SI{95}{\percent}-Umgebung der
             Messwerte.}
    \label{fig:1b}
\end{figure}

Unter der Vermutung dass der systematische Einfluss des Umgebungslichts für die
Abweichung zwischen der gemessenen und der nominellen optischen Dichte
verantwortlich ist, wurde die bei abgeschalteter Signalquelle gemessene Spannung
von den in \tab{messungen-gleichlicht} dargestellten Messwerten abgezogen. Unter
der Annahme, dass die Messwerte unkorreliert sind\footnote{Dies ist zudem die
Worst-Case-Abschätzung}, sind die Standardabweichungen der Messungen dabei zu
addieren. Die Ergebnisse sind in \fig{1b} grafisch aufgetragen.

Die Verbesserung des Ergebnisses ist deutlich erkennbar, die so korrigierten
Messwerte stimmen deutlich besser mit dem erwarteten Ergebnis überein.
Anzumerken ist jedoch auch, dass auch die Standardabweichung der korrigierten
Werte größer ist, da die Messung des Umgebungslichts ebenfalls mit Unsicherheit
behaftet ist.

\subsubsection{Diskussion}
Mit der Korrektur des systematischen Einflusses des Umgebungslichts stimmt die
detektierte Intensität des Lasers schon relativ gut mit den erwarteten Werten
überein. Da sich im verwendeten Aufbau jedoch die Intensität des Umgebungslichts
verhältnismäßig schnell ändern kann (Position von Personen im Raum,
Lichtverhältnisse draußen) ist fraglich, wie gut sich dieses Ergebnis
reproduzieren lässt.

\subsection{Messung mit Lock-In-Verfahren}

\subsubsection{Vorbereitung}
Bevor die Lock-In-Methode zur Messung der optischen Dichte der Graufilter
angewendet werden soll, ist ein ruhiger Spektralbereich und damit eine passende
Trägerfrequenz zu finden. Hierzu wird mit ausgeschalteter Signalquelle eine
Messung mit anschließender Spektralanalyse durchgeführt, dargestellt in
\fig{1c}.
Erwartungsgemäß sind im niederfrequenten Spektrum die Netzfrequenz mit
\SI{50}{\hertz} sowie einige Harmonische dieser stark vertreten, im Bereich um
\SI{200}{\kilo\hertz} liegt eine Störung durch die im Versuchslabor vorhandenen
Leuchtstoffröhren vor.
Der Bereich von \SIrange{1}{10}{\mega\hertz} ist relativ frei von externen
Einflüssen sowie vergleichsweise rauscharm, aus welchem Grund für die
Hochfrequenzmessung eine Trägerfrequenz von \SI{5}{\mega\hertz} gewählt wird.
\begin{figure}
    \centering
    \includegraphics[scale=.5]{1c-spektralbereich}
    \caption[Suche eines ruhigen Spektralbereichs]%
            {Suche eines ruhigen Spektralbereichs.
             Im oberen Spektrum ist die Netzfrequenz von \SI{50}{\hertz} sowie
             einige Harmonische dieser deutlich erkennbar, im unten
             dargestellten Spektrum fällt eine durch Leuchtstoffröhren
             verursachte Störung im Bereich um \SI{200}{\kilo\hertz} auf.
             Das Frequenzband von \SIrange{1}{10}{\mega\hertz} scheint
             \gq{ruhig} zu sein.}
    \label{fig:1c}
\end{figure}

\subsubsection{Durchführung}
\begin{sidewaysfigure}
    \centering
    \includegraphics[width=\textwidth]{1d-lock-in-blockdiagramm}
    \caption[Blockdiagramm Signalverarbeitung im Lock-In-Verstärker]%
            {Blockdiagramm der Signalverarbeitung im Lock-In-Verstärker.
             Der Laserstrahl wird mit einer im Lock-In-Verstärker erzeugten
             \SI{5}{\mega\hertz}-Schwingung moduliert, das Signal der Fotodiode
             wird darauf mit dem selben Träger sowie der um \SI{90}{\degree}
             verschobenen Trägerschwingung gemischt.
             Nach anschließender Tiefpassfilterung liegt das IQ-Signalpaar vor,
             dessen Betrag die gesuchte Messgröße darstellt.}
    \label{fig:1d-blockdiagramm}
\end{sidewaysfigure}
Die zuvor durchgeführten Messungen zur Bestimmung der optischen Dichte von
Graufiltern werden nun mit Hilfe des Lock-In-Verstärkers bei der zuvor
festgelegten Trägerfrequenz von \SI{5}{\mega\hertz} wiederholt.
Dabei wird der Laserstrahl mit der im Lock-In-Verstärker erzeugten
Sinusschwungung moduliert und das Fotodetektorsignal anschließend im Verstärker
mit der selben Schwingung wieder demoduliert. Die Signalverarbeitungskette ist
in \fig{1d-blockdiagramm} dargestellt.
Für jede Messung werden wieder der Mittelwert und die Standardabweichung bei
einer Mittelung über zehn Messungen notiert, dargestellt in
\tab{messungen-lock-in}.

\begin{table}
	\centering
	\caption{Messungen mit dem Lock-In-Verfahren}
    \label{tab:messungen-lock-in}
	\begin{tabu}{ c r r }
        \toprule
            & $ \mu $ & $ \sigma $ \\
		\midrule
            \textbf{OD0} &  50.6 \si{\milli\volt} &  17 \si{\micro\volt} \\
            \textbf{OD2} & 503   \si{\micro\volt} & 600 \si{\nano\volt} \\
            \textbf{OD3} &  48.6 \si{\micro\volt} & 400 \si{\nano\volt} \\
            \textbf{OD4} &   4.9 \si{\micro\volt} & 400 \si{\nano\volt} \\
		\bottomrule
	\end{tabu}
\end{table}

\subsubsection{Auswertung}
\begin{figure}
    \centering
    \includegraphics[scale=.5]{1d-lock-in-OD}
    \caption[Messung der optischen Dichte mit dem Lock-In-Verfahren]%
            {Messung der optischen Dichte mit dem Lock-In-Verfahren.
             Trotz sehr kleiner Signal\-amplituden stimmen die gemessene
             optische Leistung bzw. die gemessene Spannung~$U_{TIA}$
             sehr genau mit den errechneten Werten überein.}
    \label{fig:1d}
\end{figure}
Wie in \fig{1d} dargestellt entsprechen die aufgenommenen Messwerte deutlich
besser dem erwarteten Ergebnis. Selbst bei sehr kleinen Signal\-amplituden
(wenige Mikrovolt) scheint das Messverfahren sehr gute Ergebnisse zu liefern.
Anzumerken ist an dieser Stelle, dass durch das hier verwendete Messverfahren
keine Kompensation des Einflusses des Umgebungslichts erforderlich ist.

% \subsubsection{Diskussion}

\subsection{Fazit}
\begin{table}
	\centering
	\caption{aus den Messungen ermittelte optische Dichte}
    \label{tab:1e-ermittelte-OD}
    \sisetup{round-mode=places, round-precision=3}
	\begin{tabu}{ l S S S }
        \toprule
              {}
            & {Gleichlicht}
            & {Gleichlicht kompensiert}
            & {Lock-In-Verfahren} \\
        \midrule
            \textbf{OD2} & 1.948809 & 2.015388 & 2.002583 \\
            \textbf{OD3} & 2.595514 & 3.033973 & 3.017514 \\
            \textbf{OD4} & 2.769898 & 4.075366 & 4.013954 \\
        \bottomrule
	\end{tabu}
\end{table}
\begin{table}
	\centering
	\caption{Abweichung gemessene zu nomineller optischer Dichte}
    \label{tab:1e-abweichung}
    \sisetup{round-mode=places, round-precision=3}
	\begin{tabu}{ l S S S }
        \toprule
              {}
            & {Gleichlicht}
            & {Gleichlicht kompensiert}
            & {Lock-In-Verfahren} \\
        \midrule
            \textbf{OD2} & 0.051191 & 0.015388 & 0.002583 \\
            \textbf{OD3} & 0.404486 & 0.033973 & 0.017514 \\
            \textbf{OD4} & 1.230102 & 0.075366 & 0.013954 \\
        \bottomrule
	\end{tabu}
\end{table}
\begin{figure}
    \centering
    \includegraphics[scale=.5]{1e-abweichung}
    \caption[Abweichung gemessene zu nomineller optischer Dichte]%
            {Abweichung gemessene zu nomineller optischer Dichte.
             Die Gleichlichtmessung ist sehr anfällig für
             Umgebungslichteinfluss, was bei der Vermessung des Filters OD4
             bereits zu einer Abweichung von \SI{1}{\od} führte. Durch Bestimmen
             und kompensieren dieses systematischen Einflusses ließ sich das
             Gleichlicht-Messverfahren deutlich verbessern, die höchste
             Genauigkeit erreichte jedoch das Lock-In-Verfahren.
            }
    \label{fig:1e}
\end{figure}
Mit drei verschiedenen Messverfahren wurden Messungen zur optischen Dichte von
Graufiltern durchgeführt. Während bei der Messung mit Gleichlicht gerade bei
einer starken Dämpfung große Messabweichungen auftraten lieferte das
Lock-In-Verfahren auch bei geringer Signal\-amplitude ein gutes Ergebnis.

In \tab{1e-ermittelte-OD} ist die aus den Messungen ermittelte optische Dichte
der der verschiedenen Filter aufgelistet, in \tab{1e-abweichung} die Abweichung
zur nominellen OD. Diese Werte sind in \fig{1e} ebenfalls grafisch dargestellt.
Wie bereits zuvor angemerkt ist die Gleichlichtmessung sehr anfällig für
Umgebungslichteinfluss, was bei der Vermessung des Filters mit der höchsten
optischen Dichte bereits zu einer Abweichung von \SI{1}{\od} führte.
Dieser Einfluss ließ sich messen und kompensieren, was bei den hier
durchgeführten Messungen für eine deutliche Verbesserung des Ergebnisses geführt
hat, die aus den Messungen ermittelte optische Dichte des Filters liegt etwa um
eine Zehnerpotenz näher am theoretischen Wert.
Noch genauer ist jedoch das Ergebnis der Messung mit dem Lock-In-Verfahren, die
aus dieser Messung rechnerisch ermittelte optische Dichte des OD4-Filters weicht
nur noch um etwa ein Prozent von der nominellen Dichte ab.

Da die Gleichlichtmessung im Basisband abläuft, ist der Einfluss des
Funkelrauschens ($\frac{1}{f}$-Rauschen) hierbei deutlich höher als bei der
Lock-In-Messung mit \SI{5}{\mega\hertz}, was sich in dem
Signal-zu-Rausch-Verhältnis widerspiegelt.
Durch die geringe Rauschleistungsdichte im verwendeten Frequenzband sowie die
geringe Empfindlichkeit gegenüber Umgebungslicht erscheint das Lock-In-Verfahren
sehr geeignet für derartige Messungen der optischen Dichte.
