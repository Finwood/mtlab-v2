\section{Laser-Mikrofon}
Ziel diese Versuches ist die Übertragung von Audiosignalen mit Hilfe eines
Lasers und einem Lock-In-Verstärker. Dabei soll der Einfluss des für das
Lock-In-Verfahren notwendigen Tiefpassfilters auf das Audiosignal untersucht
werden.

\subsection{Aufbau}
Der Messaufbau besteht wie in \fig{2-aufbau} dargestellt aus einer Laserdiode,
deren Strahl über einen auf einer Lautsprechermembran befestigten Spiegel auf
den bereits zuvor erwähnten Fotodetektor fällt.
Wenn am Lautsprechereingang ein Signal anliegt beginnt die Membran zu schwingen,
wodurch das Signal auf den reflektierten Laserstrahl übertragen wird.
Die Laserdiode wird mit dem internen Oszillator des Lock-In-Verstärkers
moduliert, der Fotodetektor an dessen Signaleingang ansgeschlossen, sodass eine
Messung mit dem Lock-In-Verfahren möglich ist. Um das Messergebnis subjektiv
beurteilen zu können wird das demodulierte Messsignal über einen weiteren
Lautsprecher ausgegeben, darüber hinaus ist der mit einem Spiegel versehene
Lautsprecher um den Höreindruck nicht zu verfälschen in einer schallisolierenden
Glasbox montiert.
Ein PC stellt die Signalquelle für den signalgebenden Lautsprecher dar.
\begin{figure}
	\centering
	\input{2-aufbau.pdf_tex}
    \caption[Messaufbau zur optischen Übertragung von Audiosignalen]%
            {Messaufbau zur optischen Übertragung von Audiosignalen, nach
             \cite{skript}. Der Strahl einer Laserdiode wird über einen auf
             einer Lautsprechermembran befestigten Spiegel auf einen
             Fotodetektor gelenkt. Mit dem Lock-In-Verfahren wird das so
             aufgenommene Signal gemessen und über einen weiteren Lautsprecher
             ausgegeben. Um den Höreindruck des zweiten Lautsprechers nicht zu
             verfälschen ist der signalgebende Lautsprecher mit einer
             Schallisolierung versehen.}
    \label{fig:2-aufbau}
\end{figure}

\subsection{Durchführung}
Zunächst wird der schallisolierte Lautsprecher so ausgerichtet, dass der
reflektierte Laserstrahl mittig in den Fotodetektor trifft. An den
schallisolierten Lautsprecher wird zuerst ein Sinussignal mit \SI{440}{\hertz}
(Kammerton \textit{a}) angelegt, später wird darüber ein Musikstück ausgegeben.
Für beide Audiosignale wird die Grenzfrequenz Tiefpassfilters zwischen
\SIlist{200;500;1000}{\hertz} variiert und der subjektive Höreindruck notiert.

\subsubsection{Kammerton \textit{a}}
\begin{figure}
    \centering
    \includegraphics[scale=.5]{2a-spektren}
    \caption[Spektrum Kammerton \textit{a} bei verschiedenen Tiefpassfiltern]%
            {Spektrum Kammerton \textit{a} bei verschiedenen Tiefpassfiltern.
             Die graue gestrichelte Linie markiert die Frequenz des Kammertons 
             \textit{a} (\SI{440}{\hertz}), die rote Linie die jeweilige
             Grenzfrequenz des verwendeten Tiefpassfilters.
             Es ist erkennbar, dass bei Filterung mit \SI{200}{\hertz} auch die
             Spektrallinie des Nutzsignals stark gedämpft wird, sowie dass bei
             einer hohen Grenzfrequenz des Filters auch ein hoher Rauschanteil
             im Signal oberhalb des Nutzsignals vorhanden ist.}
    \label{fig:2a}
\end{figure}
Wie bereits erwähnt wird zunächst ein Sinussignal mit \SI{440}{\hertz} über den
Lautsprecher ausgegeben. Die Grenzfrequenz des für das Lock-In-Verfahren
erforderlichen Tiefpassfilters wird variiert, die bei verschiedenen
Grenzfrequenzen aufgenommenen Spektren sind in \fig{2a} dargestellt.

Deutlich erkennbar in den Spektren ist der für ein Tiefpassfilter
charakteristische Amplitudenabfall oberhalb der Grenzfrequenz. Auch gut sichtbar
ist die Spektrallinie bei \SI{440}{\hertz}, welche von dem Nutzsignal kommt. Die
anderen Spektralanteile sind in diesem Fall Rauschen und somit unerwünscht.

Bei der Filterung mit \SI{200}{\hertz}, also unterhalb der Frequenz des
Nutzsignals, wird auch das Nutzsignal bereits gedämpft. Der Ton ist zwar noch
vage zu erkennen gewesen, allerdings war der Höreindruck sehr dumpf.
Dies ist bei der Filterung mit \SI{500}{\hertz} deutlich besser gewesen, der Ton
war deutlich erkennbar und hob sich besser vom Hintergrundrauschen ab. Dies
geht auch aus dem Spektrum hervor: der Abfall der Amplitude findet erst oberhalb
der \SI{440}{\hertz} statt, die Amplitude des Kammertons ist deutlich höher als
die des umgebenden Rauschens.
Bei der Filterung mit \SI{1}{\kilo\hertz} war der Ton zwar noch etwas heller und
natürlicher als bei den voherigen Messungen zu erkennen, allerdings ist er durch
die höhere Bandbreite des Signals auch mit deutlich mehr Rauschen überlagert. 
Wie im Spektrum erkennbar ist sind deutlich mehr hochfrequente Rauschanteile im
Signal vorhanden, was den verrauschten Höreindruck widerspiegelt.

\subsubsection{Musikstück}
Bei dem abgespielten Musikstück lassen sich ähnliche Beobachtungen wie bei der
vorigen Messreihe machen.
Da die Bandbreite des Musikstücks größer als die des \SI{440}{\hertz}-Signals
ist, ist zwischen allen drei verwendeten Tiefpassfiltern ein deutlicher
Unterschied zu hören. Ist der Höreindruck bei einer Filterung mit
\SI{200}{\hertz} noch ausgesprochen dumpf und primär auf Basstöne beschränkt,
wird das Stück mit steigender Grenzfrequenz des Tiefpassfilters zunehmend
klarer, dabei steigt jedoch auch der Rausch-Anteil und bei \SI{1}{\kilo\hertz}
trat ein deutlich hörbares Klirren auf.

\subsection{Fazit}
Wenn auch nicht in erster Linie darauf ausgelegt, lässt sich mit einem
Lock-In-Verstärker auch eine optische Übertragung eines analogen Audiosignals
realisieren. Da der verwendete Aufbau sehr einfach und die Übertragungsstrecke
vergleichsweise schlecht war, ist das Messergebnis stark verrauscht gewesen.
Durch eine geeignete Filterung lässt sich das Signal-zu-Rausch-Verhältnis
verbessern, die Betonung liegt jedoch auf \textit{geeignet}: bei einer zu hohen
Bandbreite ist das gefilterte Signal unnötig stark von Rauschen überlagert, bei
einer zu geringen Bandbreite werden auch Anteile des Nutzsignals unterdrückt.
Um die Grenzfrequenz des Tiefpassfilters sinnvoll wählen zu können sind daher
Kenntnisse über das erwartete Signal vonnöten.
