\section{Laser-Abstandssensor}
In dem folgenden Versuch soll mithilfe eines Lasers und eines
Lock-In-Verstärkers über die Phasenverschiebung eine Längenmessung realisiert
werden.

\subsection{Aufbau}
Der Fotodetektor ist wie in \fig{2-aufbau} dargestellt auf einer optischen Bank
mit Längenskala verschiebbar vor einer Lasediode montiert.
Die Laserdiode wird wie im vorherigen Versuch mit einer Frequenz
von~\SI{5}{\mega\hertz} moduliert, der Fotodetektor wird wieder an den
Signaleingang des Lock-In-Verstärkers angeschlossen. In \fig{3-blockdiagramm}
ist die Signalverarbeitungskette als Blockdiagramm dargestellt.
\begin{figure}
	\centering
	\input{3-aufbau.pdf_tex}
    \caption[Messaufbau zur Längenmessung nach dem Lock-In-Verfahren]%
            {Messaufbau zur Längenmessung nach dem Lock-In-Verfahren,
             nach \cite{skript}.
             Der Fotodetektor ist verschiebbar auf einer optischen Bank mit
             Längenskala montiert, sodass sich der Abstand zwichen Lichtquelle
             und Detektor justieren lässt.}
    \label{fig:3-aufbau}
\end{figure}
\begin{sidewaysfigure}
    \centering
    \includegraphics[width=\textwidth]{3-blockdiagramm}
    \caption[Blockdiagramm Signalverarbeitung Längenmessung]%
            {Blockdiagramm der Signalverarbeitung für die Längenmessung per
             Phasendifferenz.
             }
    \label{fig:3-blockdiagramm}
\end{sidewaysfigure}

\subsection{Durchführung}
\subsubsection{Phasenverschiebung bei verschiedenen Distanzen}
Für verschiedene Positionen des Fotodetektors auf der optischen Bank werden
der Abstand~$d$ zwischen Laserdiode und Detektor und die aus der Signallaufzeit
resultierende Phasenverschiebung~$\Delta\varphi$ sowie als Maß für das
Phasenrauschen die empirische Standardabweichung~$s_{\Delta\varphi}$ notiert,
aufgelistet in \tab{phasenverschiebung}.
\begin{table}
	\centering
	\caption{Messwerte Phasenverschiebung}
    \label{tab:phasenverschiebung}
	\begin{tabu}{ S S S S }
        \toprule
            {$d$ in \si{\centi\metre}} &
            {$\Delta\varphi$ in \si{\degree}} &
            {$s_{\Delta\varphi}$ in \SI{e-3}{\degree}} &
            {$u_d$ in \si{\micro\metre}} \\
		\midrule
            15 & -23.0796 & 0.7 & 116.6 \\
            20 & -23.3808 & 0.6 &  99.9 \\
            30 & -23.9952 & 0.7 & 116.6 \\
            40 & -24.6256 & 0.8 & 133.2 \\
            50 & -25.2801 & 0.6 &  99.9 \\
            60 & -25.9509 & 0.7 & 116.6 \\
            70 & -26.6057 & 0.7 & 116.6 \\
            80 & -27.2170 & 0.6 &  99.9 \\
            90 & -27.7839 & 0.6 &  99.9 \\
        \bottomrule
	\end{tabu}
\end{table}
Mit Verringern der Grenzfrequenz des Tiefpass-Filters wird das Phasenrauschen
erwartungsgemäß geringer, da hochfrequente Anteile eines Rauschsignals stärker
gedämpft werden. Da sich die für diese Messung interessanten Informationen
aufgrund der geringen Dynamik im Basisband befinden, lässt sich die
Grenzfrequenz sehr niedrig wählen.
Für die in \tab{phasenverschiebung} aufgelisteten Messwerte wurde ein
Tiefpassfilter achter Ordnung mit einer Grenzfrequenz von~\SI{10}{\hertz}
verwendet; die gesamte Signalverarbeitungskette ist in \fig{3-blockdiagramm}
dargestellt.

\subsubsection{Unsicherheit der Positionsbestimmung}
Die Phasenverschiebung~$\Delta\varphi$ einer Welle der Frequenz~$f$ bzw. der
Wellenlänge~$\lambda=\frac{c}{f}$ auf einer Strecke~$d$ beträgt
\begin{equation*}
    \Delta\varphi = 2 \pi \cdot \frac{d}{\lambda}
\end{equation*}
bzw. bei Angabe des Winkels in Grad
\begin{equation}
    \Delta\varphi = \SI{360}{\degree} \cdot \frac{d}{\lambda} \, \text{.}
\end{equation}
Umgestellt nach $d$ ergibt sich
\begin{equation*}
    d = \lambda \cdot \frac{\Delta\varphi}{\SI{360}{\degree}} \, \text{.}
\end{equation*}
Unter der Annahme dass die Wellenlänge hinreichend genau bekannt ist lässt sich
hiermit aus der Standardabweichung~$s_{\Delta\varphi}$ der Phasenverschiebung
die Mess\-unsicherheit~$u_d$ der Längenmessung bestimmen:
\begin{equation}
    u_d = \lambda \cdot \frac{s_{\Delta\varphi}}{\SI{360}{\degree}}
\end{equation}
Bei der hier durchgeführten Messung wurde der Laser mit einer Frequenz von
\SI{50}{\mega\hertz} moduliert, was in einer Wellenlänge von \SI{59.96}{\metre}
resultiert.
Die maximale gemessene empirische Standardabweichung~$s_{\Delta\varphi}$ beträgt
\SI{0.8e-3}{\degree}, somit liegt die (höchste) Messunsicherheit bei
\begin{equation*}
    u_d = \lambda \cdot \frac{s_{\Delta\varphi}}{\SI{360}{\degree}}
        = \SI{59.96}{\metre} \cdot \frac{\SI{0.8e-3}{\degree}}{\SI{360}{\degree}}
        = \SI{133.2}{\micro\metre} \, \text{.}
\end{equation*}
Die für die verschiedenen Messwerte der empirischen Standardabweichung
berechnete Unsicherheit ist ebenfalls in \tab{phasenverschiebung} aufgelistet.

\subsubsection{Ermitteln der Proportionalitätskonstante}
\begin{figure}
    \centering
    \includegraphics[scale=.5]{3c-regression}
    \caption[Distanz über Phasenverschiebung]%
            {Distanz zwischen Laserdiode und Detektor über gemessener
             Phasenverschiebung.
             Der rechnerisch bestimmte Zusammenhang zwischen Phasenverschiebung
             und Distanz scheint nicht zu stimmen, die gemessenen Werte weichen
             systematisch von den errechneten ab.}
    \label{fig:3c}
\end{figure}
Eine lineare Regression der Messdaten lieferte eine Steigung
von~\SI{-157}{\milli\metre\per\degree}, im Gegensatz zum theoretischen Wert
von~$\frac{\lambda}{\SI{360}{\degree}} = \SI{167}{\milli\metre\per\degree}$.
Das Ergebnis sowie die Abweichung zu den gemessenen Werten ist in \fig{3c}
grafisch dargestellt.

Zunächst fallen die unterschiedlichen Vorzeichen auf. Dies ist aller
Wahrscheinlichkeit nach auf eine von den oben gemachten Annahmen abweichende
Vorzeichenkonvention im Lock-In-Verstärker zurückzuführen.
Darüber hinaus liegen die beiden Steigungen jedoch auch betragsmäßig relativ
weit auseinander, was auf mangelnde Maßtreue des Maßstabs der optischen Bank,
Ablesefehler bei der manuellen Positionierung oder mangelnde Frequenztreue des
Oszillators für die Modulationsfrequenz zurückzuführen sein könnte.

\subsection{Fazit}
Über den Phasenvergleich zwischen Mess- und Referenzsignal lässt sich mithilfe
des Lock-In-Ver\-fahrens ebenfalls eine Längenmessung umsetzen. In der Theorie ist
hiermit eine sehr hohe Auflösung realisierbar, was in dem hier verwendeten
Versuchsaufbau leider nicht beobachtbar war. Der Grund hierfür muss jedoch nicht
in dem Messverfahren liegen, sondern könnte auch in dem verwendeten Aufbau
begründet verankert sein. Um dies beurteilen zu können müssten allerdings
weitere Versuche durchgeführt werden.
