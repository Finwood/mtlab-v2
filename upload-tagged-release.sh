#!/bin/bash

API_URL="${CI_PROJECT_URL%/$CI_PROJECT_PATH}/api/v4"
PROJECT_API_URL="$API_URL/projects/$CI_PROJECT_ID"

echo $PROJECT_API_URL

TAG=$(
  curl --silent --header "PRIVATE-TOKEN: $GITLAB_PAT" \
       "$PROJECT_API_URL/repository/tags/$CI_COMMIT_TAG"
)
echo $TAG | jq .

RELEASE=$(echo $TAG | jq .release)

if [ -z "$RELEASE" ] || [ "$RELEASE" == 'null' ]
then
  METHOD=POST
  RELEASE_NOTES=""
else
  METHOD=PUT
  RELEASE_NOTES=$(echo $RELEASE | jq --raw-output .description)
  RELEASE_NOTES+="\n\n---\n"
fi

echo -e "$RELEASE_NOTES"

for release_file in $RELEASE_FILES
do
  fname="${release_file##*/}"
  tagged_file="${fname%%.*}-$CI_COMMIT_TAG.${fname#*.}"
  cp "$release_file" "$tagged_file"
  echo -n "$tagged_file  -->  "

  response=$(curl --silent --request POST \
                  --header "PRIVATE-TOKEN: $GITLAB_PAT" \
                  --form "file=@$tagged_file" \
                  "$PROJECT_API_URL/uploads")
  echo $response | jq --raw-output .url

  markdownlink=$(echo $response | jq --raw-output .markdown)
  RELEASE_NOTES+="- $markdownlink\n"

  rm "$tagged_file"
done

echo -e "$RELEASE_NOTES"

response=$(curl --silent --request $METHOD \
                --header "PRIVATE-TOKEN: $GITLAB_PAT" \
                --data "description=$(echo -e "$RELEASE_NOTES")" \
                "$PROJECT_API_URL/repository/tags/$CI_COMMIT_TAG/release")

echo "$response" | jq .
